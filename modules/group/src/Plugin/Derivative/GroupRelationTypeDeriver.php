<?php

namespace Drupal\contacts_group\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\group\Plugin\Group\Relation\GroupRelationTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Derive plugin definitions on the basis of GroupRelationType plugins.
 *
 * @package Drupal\contacts_group\Plugin\Derivative
 */
class GroupRelationTypeDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The group content enabler plugin manager.
   *
   * @var \Drupal\group\Plugin\Group\Relation\GroupRelationTypeManagerInterface
   */
  protected $relationTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    $base_plugin_id,
  ) {
    return new static(
      $container->get('group_relation_type.manager')
    );
  }

  /**
   * Group Relation Type deriver constructor.
   *
   * @param \Drupal\group\Plugin\Group\Relation\GroupRelationTypeManagerInterface $relation_type_manager
   *   The group relation type manager.
   */
  public function __construct(GroupRelationTypeManagerInterface $relation_type_manager) {
    $this->relationTypeManager = $relation_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $map = $this->relationTypeManager->getGroupTypePluginMap();
    if (!isset($map['contacts_org'])) {
      return [];
    }

    foreach ($map['contacts_org'] as $relationship_type_plugin_id) {
      /** @var \Drupal\group\Plugin\Group\Relation\GroupRelationInterface $relation */
      $relation = $this->relationTypeManager->createInstance($relationship_type_plugin_id, [
        'group_type_id' => 'contacts_org',
      ]);

      $relation_type = $relation->getRelationType();

      if ($relation_type->getEntityTypeId() !== 'user') {
        continue;
      }

      $this->derivatives[$relationship_type_plugin_id] = [
        'admin_label' => new TranslatableMarkup('Organisation @relation_type form.', [
          '@relation_type' => $relation_type->getLabel(),
        ]),
        'group_relation_type' => $relationship_type_plugin_id,
      ] + $base_plugin_definition;
    }

    return $this->derivatives;
  }

}
