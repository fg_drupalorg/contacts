<?php

namespace Drupal\contacts_group\Plugin\Field;

use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Computed group membership field list.
 */
class GroupMembershipItemList extends EntityReferenceFieldItemList {

  use ComputedItemListTrait;

  /**
   * Compute the list property from state.
   */
  protected function computeValue() {
    $this->list = [];

    // Check whether we can get a memberships for this user.
    /** @var \Drupal\user\UserInterface $entity */
    $entity = $this->getEntity();
    if ($entity->isNew()) {
      return;
    }

    // @todo change from group_content to group_relationship when moving
    // to group 3.x.
    $storage = \Drupal::entityTypeManager()->getStorage('group_content');

    /** @var \Drupal\group\Entity\GroupMembershipInterface[] $group_memberships */
    $group_memberships = $storage->loadByProperties([
      'type' => $this->getSetting('group_type') . '-group_membership',
      'entity_id' => $entity->id(),
    ]);

    // Build our list.
    $delta = 0;
    foreach ($group_memberships as $membership) {
      $this->list[$delta] = $this->createItem($delta, $membership);
      // Backwards compatibility: Expose the underlying GroupRelationship entity
      // as a property called 'membership' in addition to the underlying
      // 'entity' property.
      // This is done because for group 1.x we exposed a Membership
      // wrapper on this property, which has subsequently been deprecated in
      // group 2.x onwards. The methods from this wrapper were moved to the
      // entity so we just expose the entity directly here for compatibility
      // with callers who might be trying to access the membership property.
      // @phpstan-ignore-next-line
      $this->list[$delta]->membership = $membership;
      $delta++;
    }
  }

}
