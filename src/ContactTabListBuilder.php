<?php

namespace Drupal\contacts;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Contact tab entities.
 */
class ContactTabListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Contact tab');
    $header['id'] = $this->t('Machine name');
    $header['path'] = $this->t('Path');
    $header['weight'] = $this->t('Weight');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->accessCheck(TRUE)
      ->sort('weight');

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\contacts\Entity\ContactTabInterface $entity */
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['path'] = $entity->getPath();
    $row['weight'] = $entity->weight ?? 0;
    return $row + parent::buildRow($entity);
  }

}
